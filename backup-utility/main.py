import sys
import os
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)


def main() -> None:
    config_file_path = sys.argv[1]
    if not os.path.isfile(config_file_path):
        logger.critical(f"Config file {config_file_path} does not exists.")
        sys.exit(1)

    logger.info(f"Reading config file: {config_file_path}")

    pass


if __name__ == '__main__':
    main()
