import json
import unittest

from utils.config_validation import validate_config


class ConfigValidationTest(unittest.TestCase):

    def test_valid_config(self):
        # read config
        test_config_file_path = "./resources/test_config.json"
        with open(test_config_file_path, "r") as test_config_file:
            test_config = json.load(test_config_file)

        self.assertTrue(validate_config(test_config))
